import numpy as np

def one_hot_encoding(y):
	""" Performs one hote encoding of labels

	Args
	------------
	y : (1D array) containing labels.
	"""
	y = np.array(y - np.min(y), dtype=int)
	nb_class = np.max(y) + 1
	y_encoded = np.identity(nb_class)[y.reshape(-1)]
	return y_encoded

def shuffle(X, y):
	""" Randomly shuffles two datasets accordingly.
	"""
	permut_index = np.random.permutation(X.shape[0])
	X = X[permut_index]
	y = y[permut_index]
	return X, y

def get_batches(X, y, batch_size):
	""" Generates batches for stochastic training.
	"""
	X, y = shuffle(X, y)
	batches = []
	for k in range(len(X)//batch_size):
		data = {
		'X': X[k*batch_size: (k+1)*batch_size],
		'y': y[k*batch_size: (k+1)*batch_size],
		}
		batches.append(data)
	return batches

class LayerFunc(object):
	""" Abstract class for layers and functions of deep network.
	"""
	def __init__(self):
		pass

	def __call__(self, x, *args, **kwargs):
		return self.forward(x, *args, **kwargs)

	def forward(self, x, *args, **kwargs):
		raise NotImplementedError

	def backward(self, x):
		raise NotImplementedError