from NeuralNet import *
from sklearn.datasets import load_iris
import matplotlib.pyplot as plt 

############ Loading and preprocessing data
iris = load_iris()
X, y = iris.data, iris.target
X = (X - np.mean(X, axis=0))/np.std(X, axis=0)

scores = []
train_histories = []
print('Iris dataset loaded and standardized.')

############ Training models
nb_iter = 50
print('Computing accuracy over {} trials...'.format(nb_iter))
for it in range(nb_iter):
	X, y = shuffle(X, y)
	Xtrain, ytrain = X[:120], y[:120]
	NN = NeuralNetwork()
	NN.add_layer(DenseLayer(14, activation=Sigmoid(), p_drop=0.3))
	NN.add_layer(DenseLayer(3, activation=Softmax()))
	NN.train(Xtrain, ytrain, nb_epoch=40, batch_size=25, method="ADAM", alpha=0.2, weight_decay=1e-3,  verbose=False)
	scores.append(NN.score(X[120:], y[120:]))
	train_histories.append(NN.train_history['train_loss'])

############ Printing results and plots.
print('Mean / std accuracy obtained : {} +/- {}'.format(round(np.mean(scores),3), round(np.std(scores),3)))
mean_train_loss_over_time = np.mean(train_histories, axis=0)
plt.figure("Training loss overtime")
plt.plot(range(len(mean_train_loss_over_time)), mean_train_loss_over_time, label="Training loss")
plt.ylabel("Loss")
plt.xlabel("Iteration")
plt.legend()
plt.show()
