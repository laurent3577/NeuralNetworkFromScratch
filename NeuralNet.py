import numpy as np
from utils import *

class ReLu(LayerFunc):
	""" Rectified Linear Unit function with backward method for backpropagation.
	"""
	def __init__(self):
		super(ReLu, self).__init__()

	def forward(self, x):
		self.output = np.maximum(x,0)
		return self.output

	def backward(self, x):
		return x * (self.output>0).astype(int)

class Tanh(LayerFunc):
	""" Tanh function with backward method for backpropagation.
	"""
	def __init__(self):
		super(Tanh, self).__init__()

	def forward(self, x):
		self.output = np.tanh(x)
		return self.output

	def backward(self, x):
		return x * (1 - self.output**2)
		
class Sigmoid(LayerFunc):
	""" Sigmoid function with backward method for backpropagation.
	"""
	def __init__(self):
		super(Sigmoid, self).__init__()

	def forward(self, x):
		self.output = 1./ (1+np.exp(-x))
		return self.output

	def backward(self, x):
		return x * (1-self.output)*self.output

class Dropout(LayerFunc):
	""" Dropout function with backward method for backpropagation.
	"""
	def __init__(self, p_drop):
		"""
		Args 
		----------
		p_drop : (float) probability of dropping out activation of each node.
		"""
		super(Dropout, self).__init__()
		self.p_drop = p_drop

	def forward(self, x):
		output_dim = x.shape
		self.drop =  np.random.binomial(1, 1-self.p_drop, size=output_dim) 
		self.drop = self.drop / (1-self.p_drop) # scaling by 1 - p_drop to preserve expected output between training and testing.
		return x * self.drop

	def backward(self, x):
		return x * self.drop

class Softmax(LayerFunc):
	""" Softmax function with backward method for backpropagation.
	"""
	def __init__(self):
		super(Softmax, self).__init__()

	def forward(self, x):
		m = np.max(x, axis=1, keepdims=True) # to improve numerical stability
		self.output = np.exp(x-m)/ (np.exp(x-m).sum(axis=1,keepdims=True))
		return self.output

	def backward(self, x):
		""" Backward method implemented considering a cross entropy loss on top of it.
		"""
		return self.output - x


class DenseLayer(LayerFunc):
	""" Fully connected layer implementation.
	"""
	def __init__(self, nb_nodes, activation, p_drop = 0):
		"""
		Args
		----------
		nb_nodes : (int) number of nodes = ouput dimension of the layer.
		activation : (LayerFunc) must have callable and backward methods implemented.
		p_drop : (float) probability to use for drop out function (use 0 for no dropout).
		"""
		super(DenseLayer, self).__init__()
		self.weights = np.array([[] for i in range(nb_nodes)])
		self.bias = np.zeros(nb_nodes)
		self.output_dim = nb_nodes
		self.activation = activation
		self.drop_out = Dropout(p_drop)

	def forward(self, X, training=False):
		self.input = np.array(X)
		if training:
			return self.drop_out(self.activation(np.dot(self.weights, X.T).T + self.bias))
		else:
			return self.activation(np.dot(self.weights, X.T).T + self.bias)

	def backward(self, x):
		x = np.dot(x, self.weights)
		return x

	def init_params(self, input_dim):
		std = np.sqrt(6./ (input_dim + self.output_dim))
		self.weights = np.random.uniform(-std, std, size=(self.weights.shape[0], input_dim))
		self.bias = np.random.uniform(-std, std, size=self.bias.size)

class NeuralNetwork(object):
	""" Neural Network implementation.
	"""
	def __init__(self):
		self.layers = []
		self.train_history = {'train_loss':[]}
		self.init = False

	def __call__(self, X, training=False):
		return self.forward(X, training)

	def add_layer(self, layer):
		self.layers.append(layer)

	def init_layers(self, input_dim):
		for layer in self.layers:
			layer.init_params(input_dim)
			input_dim = layer.output_dim
		self.init = True

	def get_params(self):
		params = []
		for layer in self.layers:
			params.append([layer.weights, layer.bias])
		return np.array(params)

	def forward(self, X, training=False):
		assert self.init, "Weights have not been initialized"
		for layer in self.layers:
			X = layer(X, training=training)
		return X

	def compute_grads(self, X, y, weight_decay):
		""" Computes gradients with respect to each layer's parameters.

		Args
		----------
		X : (array-like) input data.
		y : (array-like) corresponding labels. 
		weight_decay : (float) coefficient of L2 norm regularisation of weights.

		Returns
		----------
		gradients : Ordered by level of layer. (array)
					gradients[:,0] are weights gradients.
					gradients[:,1] are bias gradients.
		loss : (float) cross entropy loss.
		"""
		out = self.forward(X, training=True)
		loss = -(np.log(out+1e-8)*y).sum(axis=1).mean()
		n = X.shape[0]

		grad_signal = y
		gradients = []
		for i,layer in enumerate(self.layers[::-1]):
			grad_signal = layer.drop_out.backward(grad_signal)
			grad_signal = layer.activation.backward(grad_signal)

			weight_grad = 1./n * np.dot(grad_signal.T, layer.input) + weight_decay * layer.weights
			bias_grad = np.mean(grad_signal, axis=0)
			gradients.append([weight_grad, bias_grad])

			grad_signal = layer.backward(grad_signal)
		return np.array(gradients[::-1]), loss

	def _train_stochastic_gradient_descent(self, X, y, nb_epoch, batch_size, alpha, weight_decay, decay_rate, verbose=True):
		if not self.init:
			self.init_layers(X.shape[1])
		if y.ndim == 1:
			y = one_hot_encoding(y)
		step = 0
		for i in range(nb_epoch):
			batches = get_batches(X, y, batch_size)
			for batch in batches:
				Xb, yb = batch['X'], batch['y']
				grads, loss = self.compute_grads(Xb,yb,weight_decay)
				self.train_history['train_loss'].append(loss)
				for k, layer in enumerate(self.layers):
					layer.weights -= alpha * grads[k][0]
					layer.bias -= alpha * grads[k][1]
				step += 1
				alpha *= np.exp(-decay_rate*step)

			if verbose:
				print("Epoch {}/{} : Loss : {}".format(i+1,nb_epoch, loss))

	def _train_adam(self, X, y, nb_epoch, batch_size, alpha, weight_decay, verbose=True):
		if not self.init:
			self.init_layers(X.shape[1])
		if y.ndim == 1:
			y = one_hot_encoding(y)

		beta1 = 0.9
		beta2 = 0.999
		epsilon = 1e-8
		init_params = self.get_params()
		
		moment1 = np.array([np.zeros(param.shape) for param in init_params])
		moment2 = np.array([np.zeros(param.shape) for param in init_params])

		step = 1
		for i in range(nb_epoch):
			batches = get_batches(X, y, batch_size)
			for batch in batches:
				Xb, yb = batch['X'], batch['y']
				grads, loss = self.compute_grads(Xb,yb,weight_decay)
				self.train_history['train_loss'].append(loss)
				previous_moment1 = moment1
				previous_moment2 = moment2
				alpha_t = alpha / np.sqrt(step)

				moment1 = previous_moment1 * beta1 + (1 - beta1) * grads
				moment2 = previous_moment2 * beta2 + (1 - beta2) * np.square(grads)
				unbiased_moment1 = moment1 / (1 - beta1 ** step)
				unbiased_moment2 = moment2 / (1 - beta2 ** step)
				sqrt_unbiased_moment2 = np.array([[np.sqrt(u) for u in v] for v in unbiased_moment2])
				update = alpha_t * unbiased_moment1 / (sqrt_unbiased_moment2 + epsilon)


				for k, layer in enumerate(self.layers):
					layer.weights -= update[k][0]
					layer.bias -= alpha * update[k][1]
				step += 1

			if verbose:
				print("Epoch {}/{} : Loss : {}".format(i+1,nb_epoch, loss))

	def train(self, X,y, nb_epoch, batch_size, method='SGD', alpha=0.01, weight_decay=1e-3, decay_rate=1e-2, verbose=True):
		""" Training optimiser wrapper. 

		Args 
		----------
		X : (array) training data.
		y : (array) corresponding labels (if ndim>1 expected to be one hot encoded).
		nb_epoch : (int) number of epoch on which to train the network.
		batch_size : (int) batch size to use during training.
		method : (str) either 'SGD' or 'ADAM'.
		alpha : (float) learning rate.
		weight_decay : (float) L2 regularisation coefficient.
		decay_rate : (float) exponential decay rate of learning rate for SGD method.
		verbose : (bool) flag for printing or not training info.
		"""
		if method=='SGD':
			self._train_stochastic_gradient_descent(X, y, nb_epoch, batch_size, alpha, weight_decay, decay_rate, verbose)
		elif method=='ADAM':
			self._train_adam(X, y, nb_epoch, batch_size, alpha, weight_decay, verbose)

	def predict(self, X):
		""" Outputs class label with highest activation.
		"""
		return np.argmax(self.forward(X, training=False), axis=1)

	def score(self, X, y):
		""" Computes accuracy of predict(X) on y. 

		Args
		----------
		X : (array) training data.
		y : (array) corresponding true labels (not one hot encoded).
		"""
		pred = self.predict(X)
		return np.mean(pred == y)



